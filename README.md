# ntl-example

Examples of [ntl ("Npm Task List")]( https://github.com/ruyadorno/ntl ) usage.

This repository is a part of 
[ntl — jeden skrypt, by wszystkimi rządzić]( http://timbercode.pl/blog/2017/04/09/ntl/ )
blog post (polish language).

## Setup

1. Install [Node.js]( https://nodejs.org/en/ )
1. Run `npm install` to download all required dependencies.
1. Run `npm install --global ntl` to have `ntl` available in command
   line.

## Examples

1. Just run `ntl` to see scripts defined in `package.json`:
   ```bash
   ntl
   ```

1. To see pre- and post-scripts, run:
   ```bash
   ntl --all
   ```

1. To see command behinds scripts, run:
   ```bash
   ntl --info
   ```

1. To run multiple commands in a sequence:
   ```bash
   ntl --multiple
   ```

1. You can also use `ntl` installed locally, instead of a global one.
   Put it inside some script in same way as with any other npm tool.
   In this repository `ntl --all --info` is hidden behind:
   ```bash
   npm start
   ```
